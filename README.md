# 文字识别OCR_UyghurTil

#### 简介
- 基于tesseract的Android端文字识别程序。可以识别维语和汉语。
- ئۇيغۇرچە خەتلىك رەسىمنى تېكىستكە ئالماشتۇرۇش پىروگراممىسى

#### 背景
学习途中了解到这个开源的文字识别OCR框架。它可以训练自己所需要的语言，之后我开始学习这方面的知识，训练了自己的识别库，并通过tess-two集成到Android端。识别率不高，接下来的工作除了进行更多的训练之外，还需要对照片进行处理，照片的斜度，角度，范围都会影响识别率。可惜我不会再有时间了。

#### 环境
Java编写的Android应用。
基于tesseract的Android库tess-two。

```
dependencies {
    implementation 'com.rmtheis:tess-two:7.0.0'
}

```


#### 相关
模型文件在\OcrTest3\app\src\main\assets目录下。

#### 截图
![ug](https://images.gitee.com/uploads/images/2020/0330/170154_1cc36f96_3040481.png "05483425a4f73970c162c0485ee4cea.png")
![ch](https://images.gitee.com/uploads/images/2020/0330/170230_b6cfd031_3040481.png "c97056410726b56ed0c9118345b0d35.png")